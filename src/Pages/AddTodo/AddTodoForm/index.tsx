import Input from '../../../shared/Input';
import Textarea from '../../../shared/Textarea';
import Button from '../../../shared/Button';
import Dropdown from '../../../shared/Dropdown';
import { priority } from './constant';
import './styles.scss';
import { useNavigate } from 'react-router-dom';
import AddFormState from '../../../state/addFormState';
import { observer } from 'mobx-react';
import { urls } from '../../../urls';

const AddTodoForm = observer(() => {
  const navigate = useNavigate();

  if (AddFormState.state.isClear) {
    navigate(urls.todo);
  }

  return (
    <div className={'add-todo-form'}>
      <Input
        onChange={AddFormState.handleChange('title')}
        value={AddFormState.state.emptyForm.title}
        name={'title'}
        sizeType={'large'}
        placeholder={'Название задачи'}
      />
      <Textarea
        onChange={AddFormState.handleChange('description')}
        value={AddFormState.state.emptyForm.description}
        name={'description'}
        placeholder={'Описание'}
      />
      <div className={'add-todo-form-container'}>
        <div className={'add-todo-form-container__item'}>
          <Input onChange={AddFormState.handleChange('date')} sizeType={'normal'} type={'date'} name={'date'} />
          <Dropdown
            name={'priority'}
            onSelect={AddFormState.handleChange('priority')}
            items={priority}
            value={AddFormState.state.emptyForm.priority}
          />
        </div>
        <Button sizeType={'normal'} styleType={'dark'} onClick={AddFormState.handleSave(navigate)}>
          Создать
        </Button>
      </div>
    </div>
  );
});

export default AddTodoForm;
