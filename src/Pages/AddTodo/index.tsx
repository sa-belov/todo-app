import { useNavigate } from 'react-router-dom';
import Button from '../../shared/Button';
import { FunctionComponent } from 'react';
import AddTodoForm from './AddTodoForm';
import './styles.scss';
import { urls } from '../../urls';

const AddTodo: FunctionComponent = () => {
  const navigate = useNavigate();

  const goBack = () => {
    navigate(urls.todo);
  };

  return (
    <div className={'add-todo'}>
      <div className={'add-todo-header'}>
        <p className={'add-todo-header__text'}>Добавить задачу</p>
        <Button sizeType={'normal'} styleType={'grey'} onClick={goBack}>
          Отмена
        </Button>
      </div>
      <div className={'add-todo__line'} />
      <AddTodoForm />
    </div>
  );
};

export default AddTodo;
