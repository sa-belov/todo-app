import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { appRoutes } from '../../routes';
import Header from './Header';
import './styles.scss';

const App = () => {
  const renderRoutes = () => {
    return appRoutes.map(({ id, path, component: Component }) => (
      <Route key={id} path={path} element={<Component />} />
    ));
  };

  return (
    <div className={'app'}>
      <Header />
      <Routes>
        {renderRoutes()}
        <Route path="*" element={<Navigate to="/todo" />} />
      </Routes>
    </div>
  );
};

export default App;
