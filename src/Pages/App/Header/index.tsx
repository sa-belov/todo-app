import './styles.scss';

const Header = () => {
  return (
    <div className={'header'}>
      <p className={'header-text'}>TODOAPP</p>
    </div>
  );
};

export default Header;
