import { useNavigate } from 'react-router-dom';
import Button from '../../shared/Button';
import React, { FunctionComponent } from 'react';
import TodoState from '../../state/todoState';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import TodoTable from './TodoTable';
import './styles.scss';
import { urls } from '../../urls';
import { observer } from 'mobx-react';

const Todo: FunctionComponent = observer(() => {
  const navigate = useNavigate();

  const AddTask = () => {
    navigate(urls.addTodo);
  };

  return (
    <div className={'todo'}>
      <Button sizeType={'large'} styleType={'dark'} onClick={AddTask}>
        Добавить задачу
      </Button>
      <Tabs className={'todo-tabs'}>
        <TabList className={'todo-tabs-list'}>
          <Tab className={'todo-tabs-list__item'}>Все</Tab>
          <div className={'todo-tabs-list__line'} />
          <Tab className={'todo-tabs-list__item'}>Текущие задачи</Tab>
          <div className={'todo-tabs-list__line'} />
          <Tab className={'todo-tabs-list__item'}>Завершённые</Tab>
        </TabList>

        <TabPanel>
          <TodoTable items={TodoState.getList()} />
        </TabPanel>
        <TabPanel>
          <TodoTable items={TodoState.getList().filter((item) => !item.resolved)} />
        </TabPanel>
        <TabPanel>
          <TodoTable items={TodoState.getList().filter((item) => item.resolved)} />
        </TabPanel>
      </Tabs>
    </div>
  );
});

export default Todo;
