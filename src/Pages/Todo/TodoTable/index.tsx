import { ITodoItem, TodoItem } from '../../../state/todoState/todoItemState';
import { FunctionComponent } from 'react';
import Button from '../../../shared/Button';
import './styles.scss';
import { observer } from 'mobx-react';
import TodoState from '../../../state/todoState';
import TodoTableItem from './TodoTableItem/idex';

interface IProps {
  items: ITodoItem[];
}

const TodoTable: FunctionComponent<IProps> = observer(({ items }) => {
  return (
    <div className={'todo-table'}>
      {items.map((item) => (
        <TodoTableItem item={item} key={item.id} />
      ))}
    </div>
  );
});

export default TodoTable;
