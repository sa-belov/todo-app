import Button from '../../../../shared/Button';
import { ITodoItem, TodoItem } from '../../../../state/todoState/todoItemState';
import TodoState from '../../../../state/todoState';
import './styles.scss';
import { FunctionComponent } from 'react';

interface IProps {
  item: ITodoItem;
}

const TodoTableItem: FunctionComponent<IProps> = ({ item }) => {
  const TodoItemState = new TodoItem(item, TodoState);
  return (
    <div className={'todo-table-card'}>
      <div className={'todo-table-card__top'}>
        <div className={'todo-table-card__top-title'}>{item.title}</div>
        <div className={'todo-table-card__top-block'}>
          <div className={'todo-table-card__top-block-priority'}>{item.priority}</div>
          <div className={'todo-table-card__top-block-date'}>{item.date}</div>
        </div>
      </div>
      <p>{item.description}</p>
      {item.resolved ? (
        <></>
      ) : (
        <div className={'todo-table-card__button'}>
          <Button sizeType={'normal'} styleType={'dark'} onClick={TodoItemState.handleResolve(item)}>
            Выполнить
          </Button>
        </div>
      )}
    </div>
  );
};

export default TodoTableItem;
