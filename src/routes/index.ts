import Todo from '../Pages/Todo';
import AddTodo from '../Pages/AddTodo';
import { IAppRoute } from './types';

export const appRoutes: IAppRoute[] = [
  { id: 'todo', path: '/todo', component: Todo },
  { id: 'addTodo', path: '/addTodo', component: AddTodo },
];
