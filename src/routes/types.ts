import { FunctionComponent } from 'react';

export interface IAppRoute {
  id: string;
  path: string;
  component: FunctionComponent;
}
