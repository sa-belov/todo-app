import { ChangeEvent, FunctionComponent, HTMLProps } from 'react';
import './styles.scss';

interface IProps extends Omit<HTMLProps<HTMLTextAreaElement>, 'onChange'> {
  onChange: (value: string, name: string) => void;
}

const Textarea: FunctionComponent<IProps> = ({ onChange, ...props }) => {
  const handleChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    onChange(e.target.value, e.target.name);
  };

  return <textarea className={'textarea'} {...props} onChange={handleChange} />;
};

export default Textarea;
