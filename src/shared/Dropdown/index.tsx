import { FunctionComponent, useRef, useState } from 'react';
import useOutsideClickHandler from '../../hooks/useOutsideClickHandler';
import './styles.scss';

interface IItem {
  id: number;
  title: string;
}

interface IProps {
  items: IItem[];
  value: string;
  onSelect: (value: string, name: string) => void;
  name: string;
}

const Dropdown: FunctionComponent<IProps> = ({ items, value, onSelect, name }) => {
  const dropdownRef = useRef<HTMLDivElement | null>(null);
  useOutsideClickHandler(dropdownRef, () => setIsOpen(false));
  const [isOpen, setIsOpen] = useState(false);
  const [isValue, setValue] = useState<string>(value);

  const changeValue = (item: IItem) => {
    setValue(item.title);
    onSelect(item.title, name);
    setIsOpen(false);
  };

  const renderItems = () => {
    return items.map((item) => (
      <div className={'dropdown-list__item'} key={item.id} onClick={() => changeValue(item)}>
        {item.title}
      </div>
    ));
  };

  return (
    <div ref={dropdownRef} className={'dropdown'}>
      <div className={'dropdown-header'} onClick={() => setIsOpen(!isOpen)}>
        {isValue}
      </div>
      <div className={'dropdown-list'}>{isOpen && <div>{renderItems()}</div>}</div>
    </div>
  );
};

export default Dropdown;
