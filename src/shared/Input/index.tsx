import { ChangeEvent, FunctionComponent, HTMLProps } from 'react';
import cn from 'classnames';
import './styles.scss';

interface IProps extends Omit<HTMLProps<HTMLInputElement>, 'onChange'> {
  onChange: (value: string, name: string) => void;
  sizeType: 'normal' | 'large';
}

const Input: FunctionComponent<IProps> = ({ sizeType, onChange, ...props }) => {
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value, e.target.name);
  };

  const inputCN = cn({
    ['input']: true,
    [`${sizeType}--size-input`]: Boolean(sizeType),
  });

  return <input className={inputCN} {...props} onChange={handleChange} />;
};

export default Input;
