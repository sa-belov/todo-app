import React, { ButtonHTMLAttributes, FunctionComponent } from 'react';
import cn from 'classnames';
import './styles.scss';

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  sizeType: 'normal' | 'large';
  styleType: 'dark' | 'grey' | 'success';
  children: React.ReactNode;
}

const Button: FunctionComponent<IProps> = ({ children, styleType, sizeType, ...props }) => {
  const buttonCN = cn({
    ['button']: true,
    [`${styleType}--button`]: Boolean(styleType),
    [`${sizeType}--size-button`]: Boolean(sizeType),
  });

  return (
    <button className={buttonCN} {...props}>
      {children}
    </button>
  );
};

export default Button;
