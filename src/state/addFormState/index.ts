import { makeAutoObservable, makeObservable } from 'mobx';
import { ITodoItem } from '../todoState/todoItemState';
import TodoState from '../todoState';
import { urls } from '../../urls';

type IndexesState = 'title' | 'description' | 'date' | 'priority';

const initAddFormState = {
  id: -1,
  resolved: false,
  title: '',
  description: '',
  date: '',
  priority: 'Приоритет',
};

class AddFormState {
  state = {
    emptyForm: initAddFormState,
    isClear: false,
  };

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  clearFields() {
    this.state.emptyForm = initAddFormState;
  }

  handleChange(name: IndexesState) {
    return (value: string) => {
      this.state.emptyForm[name] = value;
    };
  }

  handleSave(navigate: any) {
    return () => {
      TodoState.append(this.state.emptyForm);
      this.clearFields();
      this.state.isClear = true;
      //this.redirect(navigate)
    };
  }

  redirect(navigate: any) {
    navigate(urls.todo);
  }
}

export default new AddFormState();
