import { makeAutoObservable } from 'mobx';
import TodoState from '../index';

export interface ITodoItem {
  id: number;
  title: string;
  resolved: boolean;
  description: string;
  date: string;
  priority: string;
}

export class TodoItem {
  id: number;
  title: string;
  resolved: boolean;
  description: string;
  date: string;
  priority: string;
  store: typeof TodoState;

  constructor(item: ITodoItem, store: typeof TodoState) {
    makeAutoObservable(this);
    this.id = item.id;
    this.title = item.title;
    this.resolved = item.resolved;
    this.description = item.description;
    this.date = item.date;
    this.priority = item.priority;
    this.store = store;
  }

  handleResolve = (item: ITodoItem) => () => {
    const updated = new TodoItem({...item, resolved: true }, TodoState)

    const index = this.store.state.todoList.findIndex((it) => it.id === updated.id);
    if (index === -1) return;

    this.store.state.todoList[index] = updated;
  }


}
