import { makeAutoObservable } from 'mobx';
import { ITodoItem, TodoItem } from './todoItemState';

interface ITodoState {
  todoList: ITodoItem[];
  loading: boolean;
}

class TodoState {
  state: ITodoState = {
    todoList: [],
    loading: false,
  };

  constructor() {
    makeAutoObservable(this);
  }

  getList() {
    return this.state.todoList;
  }

  append(item: ITodoItem) {
    item.id = Date.now();
    const todo = new TodoItem(item, this);
    this.state.todoList.push(todo);
  }
}

export default new TodoState();
